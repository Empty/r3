package r3.rpc;

/**
 * 反射策略
 */
public abstract class InvokePolicy {

    /**
     * 反射
     * @param request
     * @return
     */
    public abstract Object invoke(RpcRequest request) throws Exception;

}
