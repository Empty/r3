package r3.rpc;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * RpcResponse
 *
 * @author zhoufn
 * @create 2017-12-25 10:32
 **/
public class RpcResponse implements Serializable {

    @Setter@Getter private String rpcId;

    @Setter@Getter private Object value;

    @Setter@Getter private Throwable exception;

}
