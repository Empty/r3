package r3.rpc;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import r3.rpc.server.NettyServer;
import r3.rpc.server.Server;

import java.util.concurrent.CountDownLatch;

/**
 * RpcServer
 *
 * @author zhoufn
 * @create 2017-12-25 10:40
 **/
@Slf4j
public class RpcServer{

    private Server server;

    @Setter@Getter private String listenHost;

    @Setter@Getter private int listenPort;

    @Setter@Getter Class<? extends InvokePolicy> invokePolicyClass;

    public RpcServer(String listenHost,int listenPort,Class<? extends InvokePolicy> invokePolicyClass){
        this.listenHost = listenHost;
        this.listenPort = listenPort;
        this.server = new NettyServer(this.listenHost,this.listenPort,invokePolicyClass);
    }

    public void start(){
        final CountDownLatch downLatch = new CountDownLatch(1);
        new Thread(()->{
            this.server.start(downLatch);

        }).start();
        try {
            downLatch.await();
        } catch (InterruptedException e) {
            log.error("netty启动失败");
        }
    }
}
