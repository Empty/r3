package r3.rpc;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * RpcRequest
 *
 * @author zhoufn
 * @create 2017-12-25 10:27
 **/
public class RpcRequest implements Serializable{

    /**
     * the id of the request
     */
    @Setter@Getter private String rpcId;

    /**
     * service bean id
     */
    @Setter@Getter private String beanId;

    /**
     * 方法名称
     */
    @Setter@Getter private String methodName;

    /**
     * 参数类型
     */
    @Setter@Getter private Class<?>[] parameterTypes;

    /**
     * 参数列表
     */
    @Setter@Getter private Object[] arguments;

}
