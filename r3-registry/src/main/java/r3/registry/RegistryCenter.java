package r3.registry;

import r3.common.R3Url;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 注册中心接口
 */
public interface RegistryCenter {

    /**
     * 初始化
     */
    void init() throws Exception;

    /**
     * 关闭
     */
    void close() throws Exception;

    /**
     * 注册
     * @param url
     */
    void registe(R3Url url) throws Exception;

    /**
     * 发现
     * @param interfce
     * @return applicationName + List
     */
    HashMap<String,HashMap<String,R3Url>> discover(String interfce) throws Exception;
}
