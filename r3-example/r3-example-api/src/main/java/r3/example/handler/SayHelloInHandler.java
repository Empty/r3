package r3.example.handler;

import r3.flow.InHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * SayHelloInHandler
 *
 * @author zhoufn
 * @create 2018-01-04 10:19
 **/
public class SayHelloInHandler extends InHandler {
    /**
     * 自定义多节点返回参数的合并
     *
     * @param results
     * @return
     * @throws Throwable
     */
    @Override
    public Object join(Object[] results) throws Throwable {
        List<String> list = new ArrayList<>();
        for(Object object : results){
            list.addAll((List<String>)object);
        }
        return list;
    }
}
