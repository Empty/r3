package r3.flow.annotation;

import r3.flow.InHandler;

import java.lang.annotation.*;

/**
 * 方法注解，用来指定方法使用的参数合流
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface In {

    /**
     * 指明需要使用的合流类
     * @return
     */
    Class<? extends InHandler> value();

}
