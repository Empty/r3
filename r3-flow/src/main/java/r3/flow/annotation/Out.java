package r3.flow.annotation;

import r3.flow.OutHandler;

import java.lang.annotation.*;

/**
 * 方法注解，用来指定方法使用的参数分流
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Out {

    /**
     *  指定实现分流的类
     */
    Class<? extends OutHandler> value();
}
