package r3.spring.rpc;

import r3.rpc.InvokePolicy;
import r3.rpc.RpcRequest;
import r3.spring.bean.ApplicationBean;
import r3.spring.bean.WorkerBean;

/**
 * SpringInvokePolicy
 *
 * @author zhoufn
 * @create 2017-12-26 11:17
 **/
public class SpringInvokePolicy extends InvokePolicy {

    /**
     * 反射
     *
     * @param request
     * @return
     */
    public Object invoke(RpcRequest request) throws Exception{
        WorkerBean targetBean = (WorkerBean) ApplicationBean.getContext().getBean(request.getBeanId());
        return targetBean.invoke(request.getMethodName(),request.getParameterTypes(),request.getArguments());
    }

}
